
function setResponseWithError(res, status, error) {
  return res.status(status).send({ code: 'error', message: error });
}

function setError(res, status, error) {
  return res.status(status).send(error);
}

function setResponse(res, data) {
  return res.status(200).send(data);
}

module.exports.setResponse = setResponse;
module.exports.setResponseWithError = setResponseWithError;
module.exports.setError = setError;
