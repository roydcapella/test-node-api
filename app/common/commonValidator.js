const logger = require('../loggers/logger.js');
const _ = require('lodash');

function isObject(value) {
  return _.isObject(value);
}

function isNull(value) {
  return _.isNull(value);
}

function isUndefined(value) {
  return _.isUndefined(value);
}

function numberOfProperty(value) {
  let size = 0;
  let key = null;
  for (key in value) {
    if (value.hasOwnProperty(key)) size += 1;
  }
  return size;
}

function isName(name) {
  return /^[a-zA-ZáéíóúüÁÉÍÓÚÜñÑ '-]+$/.test(name);
}

function isNumber(number) {
  return /^\d+$/.test(number);
}

function isCellPhone(cellNumber) {
  return /^[0-9]{9}$/.test(cellNumber);
}

function isTelephoneWithOutArea(telNumber) {
  return /^[0-9]{8}$/.test(telNumber);
}

function isAreaTelephone(areaNumber) {
  return /^[0-9]{2}$/.test(areaNumber);
}

function isRut(rutCompleto) {
  logger.debug('commonValidator.isRut(%s)', rutCompleto);

  if (!rutCompleto || rutCompleto === '' || rutCompleto === null || rutCompleto === undefined) {
    return false;
  }

  const rut = rutCompleto.replace(/\./g, '');

  if (rut.length < 2 || rut.length > 10) {
    return false;
  }

  if (!/^\d{1,8}\d{1,1}|k|K$/.test(rutCompleto)) {
    return false;
  }

  const rutWithoutDigit = rut.substring(0, rut.length - 2);
  const dv = rut.substring(rut.length - 1, rut.length);
  let suma = 0;
  let j = 1;
  for (let i = rutWithoutDigit.length; i > 0; i -= 1) {
    j += 1;
    if (j > 7) j = 2;
    suma += (rutWithoutDigit.charAt(i - 1)) * j;
  }

  const digito = 11 - (suma % 11);
  const valido = ('0123456789K0'.charAt(digito) === dv.toUpperCase());
  return valido;
}

function isEmptyString(text) {
  return _.isEmpty(text);
}

function isEmail(email) {
  const regex = /^[a-zA-Z.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/igm;
  return regex.test(email);
}

module.exports.isEmail = isEmail;
module.exports.isName = isName;
module.exports.isCellPhone = isCellPhone;
module.exports.isNumber = isNumber;
module.exports.isTelephoneWithOutArea = isTelephoneWithOutArea;
module.exports.isAreaTelephone = isAreaTelephone;
module.exports.isRut = isRut;
module.exports.isEmptyString = isEmptyString;
module.exports.numberOfProperty = numberOfProperty;
module.exports.isUndefined = isUndefined;
module.exports.isNull = isNull;
module.exports.isObject = isObject;
