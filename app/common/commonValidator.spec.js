
describe('Common Validator', () => {

    describe('isUndefined function', () => {

        it('When an object doest have an attribute return undefined should be true', function () {
            var isUndefined = require("./commonValidator").isUndefined;
            let obj ={}
            expect(isUndefined(obj.aa)).toBe(true);
        });

    });

    describe('isNull function', () => {

        it('When an object has an attribute null should return true', function () {
            var isNull = require("./commonValidator").isNull;
            let obj ={};
            obj.aa=null;
            expect(isNull(obj.aa)).toBe(true);
        });

    });

    describe('isObject function', () => {

        it('When a literal object is defined  should return true', function () {
            var isObject = require("./commonValidator").isObject;
            let obj ={};
            expect(isObject(obj)).toBe(true);
        });

        it('When a  function is defined  should return true', function () {
            var isObject = require("./commonValidator").isObject;
            let obj =function() {this.aa=function(){{}}};
            expect(isObject(new obj())).toBe(true);
        });

    });

    describe('numberOfProperty function', () => {

        it('When a literal object doesnt have properties should return 0', function () {
            var numberOfProperty = require("./commonValidator").numberOfProperty;
            let obj ={};
            expect(numberOfProperty(obj)).toBe(0);
        });

        it('When a literal object has 3 properties should return 3', function () {
            var numberOfProperty = require("./commonValidator").numberOfProperty;
            let obj ={a:1,b:2,c:3};
            expect(numberOfProperty(obj)).toBe(3);
        });

    });

    describe('isName function', () => {

        it('check name format', function () {
            var isName = require("./commonValidator").isName;
            expect(isName("Rodrigo Ñúñez D'amico-Martínez")).toBeTruthy();
            expect(isName("23455")).toBeFalsy();
            expect(isName("&")).toBeFalsy();
        });

    });

    describe('isNumber function', () => {

        it('check numbers ', function () {
            var isNumber = require("./commonValidator").isNumber;
            expect(isNumber("1234567890")).toBeTruthy();
            expect(isNumber("e")).toBeFalsy();
            expect(isNumber("&")).toBeFalsy();
        });

    });

    describe('isCellPhone function', () => {

        it('check CellPhone ', function () {
            var isCellPhone = require("./commonValidator").isCellPhone;
            expect(isCellPhone("123456789")).toBeTruthy();
            expect(isCellPhone("12345678")).toBeFalsy();
            expect(isCellPhone("1234567890")).toBeFalsy();
            expect(isCellPhone("1 2345 67")).toBeFalsy();
            expect(isCellPhone("abc")).toBeFalsy();
            expect(isCellPhone("&")).toBeFalsy();
        });

    });

    describe('isTelephoneWithOutArea function', () => {

        it('isTelephoneWithOutArea validation ', function () {
            var isTelephoneWithOutArea = require("./commonValidator").isTelephoneWithOutArea;
            expect(isTelephoneWithOutArea("12345678")).toBeTruthy();
            expect(isTelephoneWithOutArea("1234567")).toBeFalsy();
            expect(isTelephoneWithOutArea("123456789")).toBeFalsy();
        });

    });

    describe('isAreaTelephone function', () => {

        it('isAreaTelephone validation ', function () {
            var isAreaTelephone = require("./commonValidator").isAreaTelephone;
            expect(isAreaTelephone("12")).toBeTruthy();
            expect(isAreaTelephone("1")).toBeFalsy();
            expect(isAreaTelephone("123")).toBeFalsy();
        });

    });


    describe('isRut function', () => {

        it('isRut validation ', function () {
            var isRut = require("./commonValidator").isRut;
            expect(isRut("11111111-1")).toBeTruthy();
            expect(isRut("96792430-K")).toBeTruthy();
            expect(isRut("96792430-k")).toBeTruthy();
            expect(isRut("11.111.111-1")).toBeTruthy();
            expect(isRut("11111111-2")).toBeFalsy();
            expect(isRut("abc")).toBeFalsy();
        });

    });

    describe('isEmail function', () => {
        it('isEmail validation ', function () {
            let isEmail = require("./commonValidator").isEmail;
            expect(isEmail("alpha@beta.com")).toBeTruthy();
            expect(isEmail("alpha.beta@beta.com")).toBeTruthy();
            expect(isEmail("alpha.beta@beta.beta.com")).toBeTruthy();
            expect(isEmail("alpha")).toBeFalsy();
            expect(isEmail("3alpha@beta.com")).toBeFalsy();
            expect(isEmail("alpha..beta@.beta.com")).toBeFalsy();
            expect(isEmail(".alpha@.beta.com")).toBeFalsy();
        });
    });

    describe('isEmptyString function', () => {

        it('isEmptyString validation ', function () {
            var isEmptyString = require("./commonValidator").isEmptyString;
            expect(isEmptyString("")).toBeTruthy();
            expect(isEmptyString(null)).toBeTruthy();
            expect(isEmptyString("a")).toBeFalsy();
        });

    });

});
