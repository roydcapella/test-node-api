const winston = require('winston');

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
  transports: [
    new (require('winston-daily-rotate-file'))({
            filename: 'logs/-api.log',
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            colorize: true,
            json: false,
            level: process.env.LOG_LEVEL || 'silly'
        })
    ]
});
const log_format = process.env.LOG_FORMAT || 'dev';
const log_level = process.env.LOG_LEVEL || 'debug';

logger.info('logger initialized');
logger.info('Starting API... LOG_FORMAT= %s LOG_LEVEL= %s', log_format, log_level);
module.exports = logger;
