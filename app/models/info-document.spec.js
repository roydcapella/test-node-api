const InfoDocument = require("./info-document");
describe("infoDocument model", () => {

    let infoDocument = null;

    beforeEach( () => {

    });

    afterEach( () => {
        flowUserDocument = null;
    });

    it("should be an instance of InfoDocument ", () => {
        infoDocument = new InfoDocument();
        expect(infoDocument instanceof InfoDocument).toBe(true);

    });

    it("should be 7 attributes (2  internal extra)", () => {
        infoDocument = new InfoDocument();
        let cont =0;
        for (key in infoDocument.schema.paths){
            cont++;
        }
        expect(cont).toBe(7);
    });

    it("should be an attribute called id", () => {
         infoDocument = new InfoDocument();
        expect(typeof infoDocument.schema.paths.id).toBe("object");
    });

    it("should be an attribute called name", () => {
         infoDocument = new InfoDocument();
        expect(typeof infoDocument.schema.paths.name).toBe("object");
    });

    it("should be an attribute called lastName", () => {
         infoDocument = new InfoDocument();
        expect(typeof infoDocument.schema.paths.lastName).toBe("object");
    });

    it("should be an attribute called email", () => {
         infoDocument = new InfoDocument();
        expect(typeof infoDocument.schema.paths.email).toBe("object");
    });

    it("should be an attribute called date", () => {
         infoDocument = new InfoDocument();
        expect(typeof infoDocument.schema.paths.date).toBe("object");
    });


});
