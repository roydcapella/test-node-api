const express = require('express');
const helper = require('../common/commonHelper');
const logger = require('../loggers/logger');
const RetrieveController = require('../controllers/retrieve-info-controller');

const router = express.Router();

/* *****************************
  Author: Roydith Capella
  Date: 31/07/2017 - 18:00h
  Summary: This method handle the user request and invoke to controller
  especified to retrieve info
******************************** */
function handler(request, response) {
  let toReturn;
  const { id } = request.params || null;
  /* if id is a number */
  if (id != null && /^\d+$/.test(id)) {
    const ctrl = new RetrieveController();
    ctrl.getInfoById(id)
    .then((result) => {
      toReturn = helper.setResponse(response, result);
    })
    .catch((error) => {
      toReturn = helper.setResponse(response, error);
    });
  } else {
    logger.debug('Invalid parameters, body: %s', JSON.stringify(request.body));
    helper.setResponseWithError(response, 403, 'Invalid parameters');
  }
  return toReturn;
}

router.get('/info/:id', handler);

module.exports = router;
module.exports.handler = handler;
