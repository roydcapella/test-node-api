var mockery = require('mockery');
var httpMocks = require('node-mocks-http');
var EventEmitter = require("events").EventEmitter;

describe('retrieveInfo route', () => {
  let response;
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    mockery.registerMock('../../loggers/logger', {
      debug: function () {},
      error: function() {},
      info: function() {}
    });
    // mock del response
    response = httpMocks.createResponse({
      eventEmitter: EventEmitter
    });
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('When I receive a GET request', () => {
    it('Should not have errors', () => {
      // mock del controller
      class RetrieveControllerMock {
        getInfoById(id) {
          return new Promise((resolve, reject) => {
            resolve({
              _id:"597fd523a66880238e090a3a",
              id: "24521",
              name: "Alpha",
              lastName: "Beta",
              email: "alpha@beta.com",
              date:"2017-08-01T01:10:59.680Z",
              __v:0
            });
          });
        }
      }
      mockery.registerMock("../controllers/retrieve-info-controller", RetrieveControllerMock);

      // mock del request
      let request = httpMocks.createRequest({
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        params: {
          id: 24521
        }
      });

      // invocar a la ruta
      route = require("./retrieve-info-get-route");
      route.handler(request, response);
      expect(response.statusCode).toBe(200);
    });



  it('should return error when parameter is incorrect', () => {
      // mock del request
      let request = httpMocks.createRequest({
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        params: {
          id: null
        }
      });
      // invocar a la ruta
      route = require("./retrieve-info-get-route");
      route.handler(request, response);
      expect(response.statusCode).toBe(403);
      expect(response._getData().message).toBe("Invalid parameters");
    });


    it("should return an error when the controller fails", () => {
      // mock del controller
      class RetrieveControllerMock {
        getInfoById(id) {
          return new Promise((resolve, reject) => {
            reject({});
          });
        }
      }
      mockery.registerMock("../controllers/retrieve-info-controller", RetrieveControllerMock);

      let request = httpMocks.createRequest({
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        params: {
          id: 24521
        }
      });

      // invocar a la ruta
      route = require("./retrieve-info-get-route");
      route.handler(request, response);
      expect(response.statusCode).toBe(200);
      //expect(response._getData().message).toBe("message");
    });



  });
});
