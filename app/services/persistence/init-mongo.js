// Bring Mongoose into the app
const mongoose = require('mongoose');
const config = require('../../config/index');
const logger = require('../../loggers/logger');

const init = () => {
  // check if  the connection with mongo  is done
  if (mongoose.connection.readyState === 0) {
    logger.info('info', 'connecting to mongodb');
    const connection = `mongodb://${config.mongoConfig.mongoHost}/${config.mongoConfig.mongoDatabase}`;
    // const connection = 'mongodb://' + config.mongoConfig.mongoHost + '/' + config.mongoConfig.mongoDatabase;
    mongoose.connect(connection);
    mongoose.Promise = global.Promise;
    // CONNECTION EVENTS
    // When successfully connected
    mongoose.connection.on('connected', () => {
      logger.info('Mongoose default connection open to %s', 'db');
    });
    // If the connection throws an error
    mongoose.connection.on('error', (err) => {
      logger.info('Mongoose default connection %s', err);
    });
    // When the connection is disconnected
    mongoose.connection.on('disconnected', () => {
      logger.info('Mongoose default connection disconnected');
    });
    // If the Node process ends, close the Mongoose connection
    process.on('SIGINT', () => {
      mongoose.connection.close(() => {
        logger.info('Mongoose default connection disconnected through app termination');
        process.exit(0);
      });
    });
  }
};

module.exports = init;
