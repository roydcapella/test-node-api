const logger = require('../../loggers/logger');
const infoDocument = require('../../models/info-document');
const config = require('../../config/index');
// connect to mongodb.
const initMongo = require('./init-mongo');

module.exports = class InfoMongoService {

  constructor() {
    initMongo();
  }

/* **************************
  Author: Roydith Capella
  Date: 31/07/2017 - 18:00h
  Summary: This method search in database a document that matches the specified id
  Returns: A document match.
 ********************* */
  findById(id) {
    return new Promise((resolve, reject) => {
      infoDocument.findOne(this.getQueryBy('id', id))
        .then((res) => {
          logger.info('user finded: %s', JSON.stringify(res));
          resolve(res);
        })
        .catch((err) => {
          logger.error(`error in mongo, ${err}!`);
          reject(err);
        });
      setTimeout(() => {
        reject('Mongo connection error');
      }, config.mongoConfig.mongoTimeout);
    });
  }

  saveInfo(document) {
    const doc = this.setDate(document);
    const query = this.getQueryBy('id', doc.id);
    return this.createOrUpdateDocument(doc, query);
  }

  createOrUpdateDocument(document, query) {
    return new Promise((resolve, reject) => {
      infoDocument.findOneAndUpdate(query, document,
      { upsert: true, setDefaultsOnInsert: true, new: true })
      .then((doc) => {
        logger.info('user flow saved: %s', JSON.stringify(document));
        resolve(doc);
      })
      .catch((err) => {
        logger.error(`error in mongo, ${err}!`);
        reject(err);
      });
      setTimeout(() => {
        reject('Mongo connection error');
      }, config.mongoConfig.mongoTimeout);
    });
  }

  getQueryBy(field, value) {
    return { [field]: value };
  }

  setDate(document) {
    const doc = document;
    doc.date = this.getDate();
    return doc;
  }

  getDate() {
    return new Date();
  }

};
