const config = require('../config/index');
const helper = require('../common/commonHelper');
const logger = require('../loggers/logger');

const securityMiddleware = (req, res, next) => {
  let toReturn;
  if (req.headers['x-access-token'] === config.accessToken) {
    toReturn = next();
  } else {
    logger.debug('access forbidden : %s', req.headers['x-access-token']);
    toReturn = helper.setResponseWithError(res, 401, 'Forbidden');
  }
  return toReturn;
};

module.exports = securityMiddleware;
