
var mockery = require('mockery');
var sinon = require('sinon');

describe("RetrieveController module", () => {
  let RetrieveController = null;
  let retrieveController = null;
  let originalTimeout;

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('../loggers/logger.js', {
      info: (param, param2) => {},
      debug: (param) => {}}
    );

    mockery.registerMock('../../config/index', {});
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
  });

  afterEach(() => {
    RetrieveController = null;
    retrieveController = null;
    mockery.disable();
    mockery.deregisterAll();
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it("Should be an instance of RetrieveController", () => {
    RetrieveController = require('./retrieve-info-controller');
    retrieveController = new RetrieveController();
    expect(retrieveController instanceof RetrieveController).toBeTruthy();
  });

  describe("getInfoById method", () => {
    it("should return a promise ", (done) => {
      // mock del servicio de mongo
      let checkObject = {used: () => {}};

      class InfoMongoServiceMock {
        findById(id) {
          checkObject.used();
          return new Promise((resolve, reject) => {
            const InfoDTO = require("../dtos/info-dto");
            let infoDTO = new InfoDTO({
              _id:"597fd523a66880238e090a3a",
              id: "24521",
              name: "Alpha",
              lastName: "Beta",
              email: "alpha@beta.com",
              date:"2017-08-01T01:10:59.680Z",
              __v:0
            });
            resolve(infoDTO);
          });
        }
      }

      let checkObjectSpy = sinon.spy(checkObject, 'used');
      mockery.registerMock('../services/persistence/info-mongo-service', InfoMongoServiceMock);

      // instanciar el sujeto de pruebas
      RetrieveController = require('./retrieve-info-controller');
      retrieveController = new RetrieveController();

      // probar el sujeto para que de OK
      retrieveController.getInfoById('24521').then((response) => {
        expect(response.id).toBe("24521");
        expect(response.name).toBe("Alpha");
        expect(response.lastName).toBe("Beta");
        expect(response.email).toBe("alpha@beta.com");
        expect(checkObjectSpy.callCount).toBe(1);
        done();
      })
      .catch((error) => {
        throw "should be in the then block";
      });
    });

    it("should return an error ", (done) => {
      // mock del servicio de mongo
      let checkObject = {used: () => {}};

      class InfoMongoServiceMock {
        findById(id) {
          checkObject.used();
          return new Promise((resolve, reject) => {
            reject({statusCode:404, message:'An error has occurred'});
          });
        }
      }
      let checkObjectSpy = sinon.spy(checkObject, 'used');
      mockery.registerMock('../services/persistence/info-mongo-service', InfoMongoServiceMock);


            // instanciar el sujeto de pruebas
      RetrieveController = require('./retrieve-info-controller');
      retrieveController = new RetrieveController();

      // probar el sujeto para que de error
      retrieveController.getInfoById('4258').then((response) => {
        throw "should be in the catch block";
      })
      .catch((error) => {
        expect(error.statusCode).toBe(404);
        expect(error.message).toBe('An error has occurred');
        expect(checkObjectSpy.callCount).toBe(1);
        done();
      });
    });
  });
});
