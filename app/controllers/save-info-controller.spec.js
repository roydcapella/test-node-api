var mockery = require('mockery');
var sinon = require('sinon');

describe("SaveInfoController module", () => {
  let SaveInfoController = null;
  let saveInfoController = null;
  let originalTimeout;

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('../loggers/logger.js', {
      info: (param, param2) => {},
      debug: (param) => {}}
    );

    mockery.registerMock('../../config/index', {});
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
  });

  afterEach(() => {
    SaveInfoController = null;
    saveInfoController = null;
    mockery.disable();
    mockery.deregisterAll();
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it("Should be an instance of SaveInfoController", () => {
    SaveInfoController = require('./save-info-controller');
    saveInfoController = new SaveInfoController();
    expect(saveInfoController instanceof SaveInfoController).toBeTruthy();
  });

  describe("saveInfo method", () => {
    it("should return a promise ", (done) => {
      // mock del servicio de mongo
      let checkObject = {used: () => {}};

      class InfoMongoServiceMock {
        saveInfo(info) {
          checkObject.used();
          return new Promise((resolve, reject) => {
            resolve({status:"ok", response:"ok"});
            //reject("error");
          });
        }
      }

      let checkObjectSpy = sinon.spy(checkObject, 'used');
      mockery.registerMock('../services/persistence/info-mongo-service', InfoMongoServiceMock);

      // instanciar el sujeto de pruebas
      SaveInfoController = require('./save-info-controller');
      saveInfoController = new SaveInfoController();
      let Info = require('../models/info-document');
      let info =  new Info({id:"1", name:"Nombre", lastName:"Apellido", email:"napellido@correo.com", date:""});

      // probar el sujeto para que de OK
      saveInfoController.saveInfo(info).then((response) => {
        expect(response.status).toBe("ok");
        expect(response.response).toBe("ok");
        expect(checkObjectSpy.callCount).toBe(1);
        done();
      })
      .catch((error) => {
        throw "should be in the then block";
      });
    });

    it("should return an error ", (done) => {
      // mock del servicio de mongo
      let checkObject = {used: () => {}};

      class InfoMongoServiceMock {
        saveInfo(info) {
          checkObject.used();
          return new Promise((resolve, reject) => {
            reject({status:"error", response:"error"});
            // resolve({status:"ok", response:"ok"});
          });
        }
      }
      let checkObjectSpy = sinon.spy(checkObject, 'used');
      mockery.registerMock('../services/persistence/info-mongo-service', InfoMongoServiceMock);

      // instanciar el sujeto de pruebas
      SaveInfoController = require('./save-info-controller');
      saveInfoController = new SaveInfoController();
      let Info = require('../models/info-document');
      let info =  new Info({id:"1", name:"Nombre", lastName:"Apellido", email:"napellido@correo.com", date:""});

      // probar el sujeto para que de error
      saveInfoController.saveInfo(info).then((response) => {
        throw "should be in the catch block";
      })
      .catch((error) => {
        expect(error.status).toBe("error");
        expect(error.response).toBe("error");
        expect(checkObjectSpy.callCount).toBe(1);
        done();
      });
    });
  });

  describe("setDocument method", () => {
    it("should return an object with the correct information", () => {
      let Info = require('../models/info-document');
      let info =  new Info({id:"1", name:"Nombre", lastName:"Apellido", email:"napellido@correo.com"});

      SaveInfoController = require('./save-info-controller');
      saveInfoController = new SaveInfoController();
      let object = saveInfoController.setDocument(info);

      expect(object.id).toBe(info.id);
      expect(object.name).toBe(info.name);
      expect(object.lastName).toBe(info.lastName);
      expect(object.email).toBe(info.email);
    });

    it("the returned object should have 4 properties", () => {
      let Info = require('../models/info-document');
      let info =  new Info({id:"1", name:"Nombre", lastName:"Apellido", email:"napellido@correo.com"});

      SaveInfoController = require('./save-info-controller');
      saveInfoController = new SaveInfoController();

      let object = saveInfoController.setDocument(info);
      let size = 0;
      for (key in object) {
          if (object.hasOwnProperty(key)) size++;
      }
      expect(size).toBe(4);
    });
  });
});
