const InfoMongoService = require('../services/persistence/info-mongo-service');
const InfoDTO = require('../dtos/info-dto');

module.exports = class RetrieveController {
  getInfoById(id) {
    const infoMongoService = new InfoMongoService();
    return new Promise((resolve, reject) => {
      infoMongoService.findById(id)
        .then((data) => {
          const dto = new InfoDTO(data);
          resolve(dto);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
};
