const InfoDTO = require("./info-dto");
const { BaseDTO, fields } = require('dtox');

describe("InfoDTO DTO", () => {
  let infoDTO = null;

  beforeEach( () => {

  });

  afterEach( () => {
    infoDTO = null;
  });

  it("should be an instance of InfoDTO ", () => {
    infoDTO = new InfoDTO({
      _id:"597fd523a66880238e090a3a",
      id: "24521",
      name: "Alpha",
      lastName: "Beta",
      email: "alpha@beta.com",
      date:"2017-08-01T01:10:59.680Z",
      __v:0
    });
    expect(infoDTO instanceof InfoDTO).toBe(true);
  });

  it("should be an instance of BaseDTO ", () => {
    infoDTO = new InfoDTO({
      _id:"597fd523a66880238e090a3a",
      id: "24521",
      name: "Alpha",
      lastName: "Beta",
      email: "alpha@beta.com",
      date:"2017-08-01T01:10:59.680Z",
      __v:0
    });
    expect(infoDTO instanceof BaseDTO).toBe(true);
  });

  it("should be 8 attributes", () => {
    infoDTO = new InfoDTO({
      _id:"597fd523a66880238e090a3a",
      id: "24521",
      name: "Alpha",
      lastName: "Beta",
      email: "alpha@beta.com",
      date:"2017-08-01T01:10:59.680Z",
      __v:0
    });
    let count = 0;
    for(value in infoDTO) {
      count++
    }
    expect(count).toBe(8);
  });

  describe("attribute id", () => {

    it("should throw an exception", () => {
      try{
        infoDTO = new InfoDTO(
          {
            _id:"597fd523a66880238e090a3a",
            id: "24521",
            name: "Alpha",
            lastName: "Beta",
            email: "alpha@beta.com",
            date:"2017-08-01T01:10:59.680Z",
            __v:0
          }
        );
      } catch (err) {
        //console.info(err);
        expect(err.name).toBe("InvalidPropertyError");
        expect(err.message).toBe("Property of type number required");
      }
    });

    it("should return the correct value", () => {
      try{
        infoDTO = new InfoDTO({
              _id:"597fd523a66880238e090a3a",
              id: "24521",
              name: "Alpha",
              lastName: "Beta",
              email: "alpha@beta.com",
              date:"2017-08-01T01:10:59.680Z",
              __v:0
            });
        expect(infoDTO.id).toBe('24521');
      } catch (err) {
        //console.info(err);
        throw  "should be in try";
      }
    });

  });

  describe("attribute name", () => {
    it("should throw an excption", () => {
      try{
        infoDTO = new InfoDTO(
          {
                _id:"597fd523a66880238e090a3a",
                id: "24521",
                name: 32132,
                lastName: "Beta",
                email: "alpha@beta.com",
                date:"2017-08-01T01:10:59.680Z",
                __v:0
          }
        );
      } catch (err) {
        //console.info(err);
        expect(err.name).toBe("InvalidPropertyError");
        expect(err.message).toBe("Property of type string required");
      }
    });

    it("should return the correct value", () => {
      try{
        infoDTO = new InfoDTO(
          {
                _id:"597fd523a66880238e090a3a",
                id: "24521",
                name: "Alpha",
                lastName: "Beta",
                email: "alpha@beta.com",
                date:"2017-08-01T01:10:59.680Z",
                __v:0
          }
        );
        expect(infoDTO.name).toBe("Alpha");
      } catch (err) {
        //console.info(err);
        throw  "should be in try";
      }
    });

  });

  describe("attribute lastName", () => {

    it("should throw an excption", () => {
      try{
        infoDTO = new InfoDTO(
          {
                  _id:"597fd523a66880238e090a3a",
                  id: "24521",
                  name: "Alpha",
                  lastName: [],
                  email: "alpha@beta.com",
                  date:"2017-08-01T01:10:59.680Z",
                  __v:0
            }
          );
      } catch (err) {
        //console.info(err);
        expect(err.name).toBe("InvalidPropertyError");
        expect(err.message).toBe("Property of type string required");
      }
    });

    it("should return the correct value", () => {
      try{
        infoDTO = new InfoDTO(
          {
                  _id:"597fd523a66880238e090a3a",
                  id: "24521",
                  name: "Alpha",
                  lastName: "Beta",
                  email: "alpha@beta.com",
                  date:"2017-08-01T01:10:59.680Z",
                  __v:0
            }
        );
        expect(infoDTO.lastName).toBe("Beta");
      } catch (err) {
        //console.info(err);
        throw  "should be in try";
      }
    });

  });

  describe("attribute email", () => {

    it("should throw an exception", () => {
      try{
        infoDTO = new InfoDTO(
          {
                  _id:"597fd523a66880238e090a3a",
                  id: "24521",
                  name: "Alpha",
                  lastName: "Beta",
                  email: "theata.com",
                  date:"2017-08-01T01:10:59.680Z",
                  __v:0
            }
        );
      } catch (err) {
        expect(err.name).toBe("Error");
        expect(err.message).toBe("Email is invalid");
      }
    });

    it("should return the correct value", () => {
      try{
        infoDTO = new InfoDTO(
              {
                      _id:"597fd523a66880238e090a3a",
                      id: "24521",
                      name: "Alpha",
                      lastName: "Beta",
                      email: "alpha@beta.com",
                      date:"2017-08-01T01:10:59.680Z",
                      __v:0
                });
        expect(infoDTO.email).toBe("alpha@beta.com");
      } catch (err) {
        throw  "should be in try";
        //console.info(err);
      }
    });

  });



});
