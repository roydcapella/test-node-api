const isEmail = require('../common/commonValidator').isEmail;
const { BaseDTO, fields } = require('dtox');

const INFO_RETRIEVED_MAPPING = {
  _id: fields.generic(),
  id: fields.string(),
  __v: fields.number(),
  name: fields.string(),
  lastName: fields.string(),
  email: fields.generic({ callback: (data) => {
    if (!isEmail(data.email)) throw new Error('Email is invalid');
  } }),
  date: fields.date(),
};

module.exports = class InfoDTO extends BaseDTO {
  constructor(data) {
    super(data, INFO_RETRIEVED_MAPPING);
  }
};
