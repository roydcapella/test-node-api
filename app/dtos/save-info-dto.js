const isRut = require('../common/commonValidator').isRut;
const { BaseDTO, fields } = require('dtox');

const SAVE_INFO_MAPPING = {
  id: fields.number(),
  name: fields.string(),
  lastName: fields.string(),
  email: fields.string(),
  test: fields.generic({ callback: (data) => {
    if (!isRut(data.test)) throw new Error('error with params');
  } }),
};

module.exports = class SaveInfoDTO extends BaseDTO {
  constructor(data) {
    super(data, SAVE_INFO_MAPPING);
  }
};
